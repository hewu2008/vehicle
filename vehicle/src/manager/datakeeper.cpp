﻿#include <QDateTime>
#include "manager/config_manager.h"
#include "manager/datakeeper.h"

DataKeeper::DataKeeper()
{
    has_last_frame = false;
}

DataKeeper *DataKeeper::get_instance() {
    static DataKeeper instance;
    return &instance;
}

void DataKeeper::set_last_frame_result(FrameResultEntity frame_result) {
    this->frame_result = frame_result;
    has_last_frame = true;
}

bool DataKeeper::check_last_frame() {
   return has_last_frame;
}

void DataKeeper::clear_last_frame() {
   has_last_frame = false;
}

FrameResultEntity* DataKeeper::get_last_frame_result() {
    if (has_last_frame == false) {
        return NULL;
    }
    return &frame_result;
}

ResultEntity* DataKeeper::get_result() {
    result.time_cycle = ConfigManager::getInstance()->get_ecb_config()->time_cycle;
    result.current_time = QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss");
    return &result;
}


void DataKeeper::clear_result() {
    result.vehicle_num = 0;
    result.vehicle_speed = 0;
    result.vehicle_types.passenger_cars = 0;
    result.vehicle_types.trucks = 0;
    result.vehicle_types.other = 0;
    result.current_time = "";
}
