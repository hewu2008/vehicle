#ifndef KCFTHREAD_H
#define KCFTHREAD_H

#include <QThread>
#include <QSharedPointer>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>

using namespace cv;

class KCFThread : public QThread
{
    Q_OBJECT

public:
    KCFThread();

public slots:
    void on_kcf_update(QSharedPointer<Mat> sp_frame, bool is_ssd_frame);

protected:
    void run();
};

#endif // KCFTHREAD_H
