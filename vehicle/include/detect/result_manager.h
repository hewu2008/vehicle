﻿#ifndef RESULTMANAGER_H
#define RESULTMANAGER_H

#include <QObject>
#include <QSharedPointer>
#include <QMutex>
#include "kcf/kcftracker.hpp"
#include "kcf/kcf_thread.h"
#include "entity/algorithm_entity.h"

class ResultManager : public QObject
{
    Q_OBJECT

public:
    static ResultManager *get_instance();

    void init();

    void detect(QSharedPointer<Mat> sp_frame, std::vector<AlgorithmResultEntity> &result);

    int get_next_result_id();

    void update_buffer_kcf_async(QSharedPointer<Mat> sp_frame, bool is_ssd_frame);

    void update_buffer_kcf(cv::Mat &frame, bool is_ssd_frame);

    int update_result_buffer(AlgorithmResultEntity *result, double &similar);

    void add_result_buffer(AlgorithmResultEntity *result);

    void clear_match_status();

signals:
    void sig_update_kcf(QSharedPointer<Mat> sp_frame, bool is_ssd_frame);

private:
    ResultManager();

    ~ResultManager();

    int get_orientation(AlgorithmResultEntity *point1, AlgorithmResultEntity *point2);

    double get_speed(AlgorithmResultEntity *point1, AlgorithmResultEntity *point2);

    int get_mass(AlgorithmResultEntity *point);

private:
    std::vector<AlgorithmResultEntity> m_result_buffer;

    int m_next_result_id;

    QMutex m_mutex;

    QSharedPointer<KCFTracker> sp_tracker;

    KCFThread *ptr_kcf_thread;
};

#endif // RESULTMANAGER_H
