#ifndef ALGORITHMMANAGER_H
#define ALGORITHMMANAGER_H

#include <QObject>
#include <QSharedPointer>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>

#include "detect/detector_thread.h"

using namespace cv;

class AlgorithmManager : public QObject
{
    Q_OBJECT

public:
    static AlgorithmManager *get_instance();

    bool init();

    void detect(QSharedPointer<Mat> sp_frame);

signals:
    void sig_detect(QSharedPointer<Mat> sp_frame);

public slots:
    void on_result(QSharedPointer<Mat> sp_frame);

    void on_result_detail(QSharedPointer<Mat> sp_frame, std::vector<ObjectEntity> objects);

private:
    AlgorithmManager();
    ~AlgorithmManager();

private:
    string model_file;
    string weights_file;
    string mean_file;
    string mean_value;
    float confidence_threshold;

    DetectorThread *detector_thread;
};

#endif // ALGORITHMMANAGER_H
