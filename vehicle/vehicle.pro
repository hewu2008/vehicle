QT += core
QT -= gui

TARGET = vehicle
CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app

INCLUDEPATH += include \
    /usr/local/cuda/include

HEADERS += \
    include/constant.h \
    include/util/utils.h \
    include/util/run_guard.h \
    include/util/common_utils.h \
    include/camera/camera.h \
    include/entity/mqtt_entity.h \
    include/entity/algorithm_entity.h \
    include/mqtt/mqtt_client_thread.h \
    include/mqtt/mqtt_client_config.h \
    include/mqtt/mqtt_client_command.h \
    include/mqtt/mqtt_client_status.h \
    include/mqtt/mqtt_client_human.h \
    include/mqtt/mqtt_client_result.h \
    include/mqtt/mqtt_client_picture.h \
    include/mqtt/mqtt_client_video.h \
    include/mqtt/mqtt_client_update.h \
    include/mqtt/mqtt_client_update_result.h \
    include/manager/json_parser.h \
    include/manager/mqtt_manager.h \
    include/manager/mqtt_observer.h \
    include/manager/config_manager.h \
    include/manager/camera_manager.h \
    include/manager/mock_manager.h \
    include/manager/timer_manager.h \
    include/manager/algorithm_manager.h \
    include/manager/datakeeper.h \
    include/manager/calibrate_manager.h \
    include/update/updater.h \
    include/detect/detector.h \
    include/detect/detector_thread.h \
    include/detect/result_manager.h \
    include/kcf/ffttools.hpp \
    include/kcf/fhog.hpp \
    include/kcf/kcftracker.hpp \
    include/kcf/labdata.hpp \
    include/kcf/recttools.hpp \
    include/kcf/tracker.h \
    include/mqtt/mqtt_client_calibrate.h \
    include/kcf/kcf_thread.h

SOURCES += main.cpp \
    src/util/utils.cpp \
    src/util/run_guard.cpp \
    src/util/common_utils.cpp \
    src/camera/camera.cpp \
    src/mqtt/mqtt_client_thread.cpp \
    src/mqtt/mqtt_client_config.cpp \
    src/mqtt/mqtt_client_command.cpp \
    src/mqtt/mqtt_client_status.cpp \
    src/mqtt/mqtt_client_human.cpp \
    src/mqtt/mqtt_client_result.cpp \
    src/mqtt/mqtt_client_picture.cpp \
    src/mqtt/mqtt_client_video.cpp \
    src/mqtt/mqtt_client_update.cpp \
    src/mqtt/mqtt_client_update_result.cpp \
    src/manager/json_parser.cpp \
    src/manager/mqtt_manager.cpp \
    src/manager/mqtt_observer.cpp \
    src/manager/config_manager.cpp \
    src/manager/camera_manager.cpp \
    src/manager/mock_manager.cpp \
    src/manager/timer_manager.cpp \
    src/manager/algorithm_manager.cpp \
    src/manager/datakeeper.cpp \
    src/manager/calibrate_manager.cpp \
    src/update/updater.cpp \
    src/detect/detector.cpp \
    src/detect/detector_thread.cpp \
    src/detect/result_manager.cpp \
    src/kcf/fhog.cpp \
    src/kcf/kcftracker.cpp \
    src/mqtt/mqtt_client_calibrate.cpp \
    src/kcf/kcf_thread.cpp

LIBS += -Llib lib/libmosquittopp.so.1 lib/libmosquitto.so.1 lib/libcaffe.so.1.0.0-rc3 \
    -lssl -lcrypto -lcares -lopencv_core -lopencv_highgui -lopencv_imgproc -lglog -lgflags \
    -lprotobuf -lboost_system -lboost_filesystem -lboost_regex

release { OBJECTS_DIR = release }
debug {	OBJECTS_DIR = debug }

MOC_DIR = $${OBJECTS_DIR}
DESTDIR = ../bin
