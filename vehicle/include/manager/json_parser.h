#ifndef JSONPARSER_H
#define JSONPARSER_H

#include <QJsonObject>
#include <QJsonValue>
#include <QJsonDocument>
#include "entity/mqtt_entity.h"
#include "entity/algorithm_entity.h"

class JsonParser
{
public:
    JsonParser();

    static ConfigEntity parse_config(QJsonObject object);

    static CommandEntity parse_command(QJsonObject object);

    static CalibrateEntity parse_calibrate(QJsonObject object);

    static QString json_status(StatusEntity status);

    static QJsonObject json_vehicle(VehicleType vehicleType);

    static QString json_result(ResultEntity result);

    static QString json_pedestrian(std::vector<AlgorithmResultEntity> *v_result);

};

#endif // JSONPARSER_H
