#ifndef UTILS_H
#define UTILS_H

class Utils
{
public:
    Utils();

    static void untar_archive();

    static void start_app();
};

#endif // UTILS_H
