﻿#ifndef MQTTENTITY_H
#define MQTTENTITY_H

#include <QString>
#include <QPoint>

#include "constant.h"

struct AppConfig {
    QString broker;
    int port;
    int start_delay;
    int algorithm_type;
    double confidence;
    bool show_video;
    bool show_detect;
    bool live_camera;
    QString rtsp_camera;
};

struct ConfigEntity {
    long time_cycle;
    long status_interval;
    QString picture_format;
    QString video_resolution;
    QString target_points;
    QString prespective_points;
};

struct CommandEntity
{
	QString type;
	long length;
};

struct StatusEntity {
    int time_cycle;
    int status;
    int mode;
    QString version;
};

struct VehicleType {
    int passenger_cars;
    int trucks;
    int other;
};

struct ResultEntity {
    int vehicle_num;
    int time_cycle;
    VehicleType vehicle_types;
    int traffic_condition;
    int vehicle_speed;
    QString weather;
    QString current_time;
};

// 标定数据实体类
struct CalibrateEntity {
    QPoint target_points[CALIBRATE_POINT_COUNT];
    QPoint prespective_points[CALIBRATE_POINT_COUNT];
};

class MqttEntity
{
public:
    MqttEntity();
};

#endif // MQTTENTITY_H
