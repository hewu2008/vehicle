﻿#ifndef DETECTORTHREAD_H
#define DETECTORTHREAD_H

#include <QThread>
#include <QSharedPointer>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>

#include "detect/detector.h"
#include "entity/algorithm_entity.h"

using namespace cv;

class AppConfig;

class DetectorThread : public QThread
{
    Q_OBJECT
public:
    DetectorThread(const string& model_file, const string& weights_file,
                   const string& mean_file, const string& mean_value);
    ~DetectorThread();

signals:
    void sig_result(QSharedPointer<Mat> sp_frame);

    void sig_result_detail(QSharedPointer<Mat> sp_frame, std::vector<ObjectEntity> objects);

public slots:
    void on_detect(QSharedPointer<Mat> sp_frame);

protected:
    void run();

    AlgorithmResultEntity get_result_entity(vector<float> &ssd_result, QSharedPointer<Mat> sp_frame);

    void update_result(std::vector<AlgorithmResultEntity> frame_results, QSharedPointer<Mat> sp_frame,
                       AppConfig *ptr_app_config);

private:
    Detector *detector;
    string model_file;
    string weights_file;
    string mean_file;
    string mean_value;
};

#endif // DETECTORTHREAD_H
