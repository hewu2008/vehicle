#!/bin/sh

# make vehicle
cd ../vehicle
qmake 
make clean 
make 

# make updater
cd ../updater
qmake 
make clean 
make 

# package tar file
cd ../bin
version=$(grep version ../bin/version.txt | cut -d= -f2)
filename="../cas_vpds_v$version.tar.gz"
tar -zcvf $filename *
checksum=$(md5sum $filename)
echo "md5sum $checksum finished"
