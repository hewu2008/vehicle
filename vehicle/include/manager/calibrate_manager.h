﻿#ifndef CALIBRATE_MANAGER_H
#define CALIBRATE_MANAGER_H

#include <opencv2/opencv.hpp>
#include <QSharedPointer>

#include "entity/mqtt_entity.h"

class CalibrateManager
{
public:
    static CalibrateManager *get_instance();

    // init calibrate transform matrix
    bool init(std::vector<cv::Point2f> *src, std::vector<cv::Point2f> *dst);

    // 重新初始化转换矩阵
    bool reinit(CalibrateEntity calibrate);

    // get perspective image from origin camera image
    QSharedPointer<cv::Mat> wrap_perspective(cv::Mat *src);

    // get transform vector points
    std::vector<cv::Point2f> get_trans_ponits(std::vector<cv::Point2f> *v_points);
private:
    CalibrateManager();

    ~CalibrateManager();

    cv::Mat m_trans_mtx;
};

#endif
