QT += core
QT -= gui

TARGET = updater
CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app

INCLUDEPATH += include

HEADERS += \
    include/util/utils.h

SOURCES += main.cpp \
    src/util/utils.cpp

release { OBJECTS_DIR = release }
debug {	OBJECTS_DIR = debug }

MOC_DIR = $${OBJECTS_DIR}
DESTDIR = ../bin
