#ifndef SUBSCRIBER_H
#define SUBSCRIBER_H

#include <QString>
#include <QThread>
#include <mosquittopp.h>

#include "constant.h"

struct MqttConfig {
    QString id;
    QString topic;
    QString host;
    int port;
    int keepalive;
};

class MqttClient : public mosqpp::mosquittopp
{
public:
    MqttClient(const char *id, const char *topic, const char *host, int port);

    ~MqttClient();

    MqttConfig* get_config();

    void connect_broker();

    void subscribe_topic();

    void publish_topic(void *payload, long payload_len, bool retain = false);

private:
    MqttConfig config;
};


class MqttClientThread : public QThread
{
public:
    MqttClientThread(MqttClient *client);

    MqttClient* get_client();

    void run();

private:
    MqttClient *client;
};

#endif // SUBSCRIBER_H
