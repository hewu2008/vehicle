### v0.0.1
1. add qt support
2. add mosquitto client

### v0.0.2
1. add MqttManager and MqttObserver
2. add jetson/config interface

### v0.0.3
1. finish all interface exception video

### v0.0.4
1. finish caffe base vehicle and pedestrian detection

### v0.1.0
1. finish first time joint testing

### v0.1.1
1. add ota update feature
2. add camera reconnection
3. refine autostart script

### v0.1.3
1. 完成自动升级的调试
2. 发布磁盘镜像文件

### v0.1.4
1. 增加行人的检测
2. 增加行人检测结果的上报
3. 增加图像的本地透视转换

### v0.1.5
1. 完成行人检测所有功能的开发

### v0.2.1
1. 增加在线更新转换矩阵
2. 使用glog进行日志打印
3. 降低识别频率,提高采样频率

### v0.2.2
1. 完成去重的优化

### v0.2.3
1. 使用logratate进行日志的管理

### v0.2.4
1. 切换为车辆检测系统
2. 增加去重功能, 上报的接口改为车辆的总计数

### clone tx2 ssd
```
sudo ./flash.sh -r -k APP -G my_backup.img jetson-tx2 mmcblk0p1
sudo ./flash.sh -r jetson-tx2 mmcblk0p1

### gstreamer
```
gst-launch-1.0 -vvv rtspsrc location=rtsp://admin:Cms123456@192.168.1.64:554/Streaming/Channels/1 latency=0 ! rtph264depay ! h264parse | decodebin ! videoconvert ! xvimagesink

gst-launch-1.0 -vvv rtspsrc location=rtsp://admin:Cms123456@192.168.1.64:554/Streaming/Channels/3 latency=0 ! rtph265depay ! h265parse ! decodebin ! videoconvert ! xvimagesink

gst-launch-1.0 -e rtspsrc location=rtsp://admin:Cms123456@192.168.1.64:554/Streaming/Channels/3 ! decodebin ! videorate ! x264enc ! avimux ! filesink location=video.mp4
```

### logratate
1. 拷贝 /etc/cron.daily/logrotate至/etc/cron.hourly/保证每小时都启动一次
2. 编辑 /etc/logrotate.conf, 添加循环日志
/home/nvidia/log/*.log {
    missingok
    hourly
    rotate 5
    dateext
    dateformat -%Y-%m-%d-%s
    compress
    copytruncate
    create 0660 nvidia nvidia
}
