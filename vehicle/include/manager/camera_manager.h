#ifndef CAMERAMANAGER_H
#define CAMERAMANAGER_H

#include <QObject>
#include <QTime>
#include <QSharedPointer>

#include "camera/camera.h"
#include "entity/mqtt_entity.h"


enum CommandStatus {
    START,
    RECORD,
    IDLE
};

class CameraManager : public QObject
{
    Q_OBJECT

public:
    static CameraManager *get_instance();

    bool init();

    void start();

    void stop();

    void do_cmd(CommandEntity cmd);

public slots:
    void on_image(QSharedPointer<Mat> sp_frame);

private:
    CameraManager();

    Camera camera;

    CommandStatus cmd_status;

    CommandEntity cmd;

    cv::VideoWriter video_writer;

    uint record_start_time;

    QTime time;
};

#endif // CAMERAMANAGER_H
