#include <glog/logging.h>
#include <QJsonDocument>

#include "entity/mqtt_entity.h"
#include "manager/json_parser.h"
#include "manager/mqtt_observer.h"
#include "mqtt/mqtt_client_video.h"

MqttClientVideo::MqttClientVideo(const char *id, const char *topic, const char *host, int port)
    : MqttClient(id, topic, host, port)
{
}

void MqttClientVideo::on_connect(int rc) {
    LOG(INFO) << "MqttClientVideo | on_connect | rc" << rc;
}

void MqttClientVideo::on_message(const struct mosquitto_message *message) {
    LOG(INFO) << "MqttClientVideo | on_message";
}

void MqttClientVideo::on_subscribe(int mid, int qos_count, const int *granted_qos) {
    LOG(INFO) << "MqttClientVideo | on_subscribe Success";
}
