#include <glog/logging.h>
#include <QJsonDocument>

#include "entity/mqtt_entity.h"
#include "manager/json_parser.h"
#include "manager/mqtt_observer.h"
#include "include/mqtt/mqtt_client_status.h"

#include "manager/mqtt_manager.h"

MqttClientStatus::MqttClientStatus(const char *id, const char *topic, const char *host, int port)
    : MqttClient(id, topic, host, port)
{
}

void MqttClientStatus::on_connect(int rc) {
    LOG(INFO) << "MqttClientStatus | on_connect | rc" << rc;
}

void MqttClientStatus::on_message(const struct mosquitto_message *message) {
    LOG(INFO) << "MqttClientStatus | on_message";
}

void MqttClientStatus::on_subscribe(int mid, int qos_count, const int *granted_qos) {
    LOG(INFO) << "MqttClientStatus | on_subscribe Success";
}
