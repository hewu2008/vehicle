#include <glog/logging.h>
#include <QJsonDocument>

#include "entity/mqtt_entity.h"
#include "manager/json_parser.h"
#include "manager/mqtt_observer.h"
#include "mqtt/mqtt_client_command.h"

MqttClientCommand::MqttClientCommand(const char *id, const char *topic, const char *host, int port)
    : MqttClient(id, topic, host, port)
{
}

void MqttClientCommand::on_connect(int rc) {
    LOG(INFO) << "MqttClientCommand | on_connect | rc" << rc;
    if (rc == 0) {
        this->subscribe_topic();
    }
}

void MqttClientCommand::on_message(const struct mosquitto_message *message) {
    char *payload = (char*)message->payload;
    LOG(INFO) << "MqttClientCommand | on_message | payload" << payload;
    QJsonDocument jsonResponse = QJsonDocument::fromJson(payload);
    if (jsonResponse.isEmpty() == true) {
        LOG(INFO) << "MqttClientCommand | on_message | payload is not json object";
        return;
    }
    CommandEntity command = JsonParser::parse_command(jsonResponse.object());
    LOG(INFO) << "MqttClientCommand | on_message | type" << command.type.toStdString() <<
                ", length" << command.length;
    MqttObserver::get_instance()->notify_command(command);
}

void MqttClientCommand::on_subscribe(int mid, int qos_count, const int *granted_qos) {
    LOG(INFO) << "MqttClientCommand | on_subscribe Success";
}
