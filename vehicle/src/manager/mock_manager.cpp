﻿#include <glog/logging.h>

#include "entity/mqtt_entity.h"
#include "manager/mqtt_manager.h"
#include "manager/json_parser.h"
#include "manager/camera_manager.h"
#include "manager/config_manager.h"
#include "manager/timer_manager.h"
#include "manager/calibrate_manager.h"
#include "manager/mock_manager.h"

MockManager::MockManager()
{
    MqttObserver::get_instance()->set_listener(this);
    updater = NULL;
}

MockManager::~MockManager() {
    if (updater != NULL) {
        delete updater;
        updater = NULL;
    }
}

MockManager* MockManager::get_instance() {
    static MockManager instance;
    return &instance;
}

void MockManager::test_status_interface() {
    LOG(INFO) << "-----------------------------------";
    LOG(INFO) << "MockManager | test_status_interface";

    ConfigEntity * pConfig = ConfigManager::getInstance()->get_ecb_config();

    StatusEntity status;
    status.time_cycle = pConfig->time_cycle;
    status.status = 0;
    status.mode = 0;

    MqttManager::getInstance()->send_status(&status);
}

void MockManager::test_result_interface() {
    LOG(INFO) << "-----------------------------------";
    LOG(INFO) << "MockManager | test_result_interface";

    ResultEntity result;
    result.vehicle_num = 10;

    result.vehicle_types.passenger_cars = 5;
    result.vehicle_types.trucks = 3;
    result.vehicle_types.other = 4;

    result.traffic_condition = 4;
    result.vehicle_speed = 60;
    result.weather = "sunny";
    result.current_time = "2017-09-09 12:11:12";

    MqttManager::getInstance()->send_result(&result);
}

/**
 * ./mosquitto_pub -t jetson/command -t jetson/config
 * -m "{\"time_cycle\":6000, \"picture_format\":\"jpg\", \"video_resolution\":\"360\"}"
 */
void MockManager::on_config(ConfigEntity config) {
    LOG(INFO) << "MockManager | on_config";
    ConfigEntity * pConfig = ConfigManager::getInstance()->get_ecb_config();
    TimerManager::get_instance()->reset_all_timer();
}

/**
 * ./mosquitto_pub -t jetson/command -t jetson/command
 * -m "{\"type\":\"picture\"}"
 */
void MockManager::on_command(CommandEntity command) {
    LOG(INFO) << "MockManager || on_command";
    CameraManager::get_instance()->do_cmd(command);
}

void MockManager::on_update(char *archive, int len) {
    LOG(INFO) << "MockManager | on_update | len" << len;
    if (updater != NULL) {
        delete updater;
    }
    updater = new Updater(archive, len);
    updater->update();
}

/**
 * ./mosquitto_pub -t ecb/camera/calibration -m "{\"current\":[{\"sequence\":0, \"longitudinalPosition\":11, \"lateralPosition\":22 }, {\"sequence\":1, \"longitudinalPosition\":33, \"lateralPosition\":44 }, {\"sequence\":2, \"longitudinalPosition\":55, \"lateralPosition\":66 }, {\"sequence\":3, \"longitudinalPosition\":77, \"lateralPosition\":88 } ], \"target\":[{\"sequence\":0, \"longitudinalPosition\":99, \"lateralPosition\":111 }, {\"sequence\":1, \"longitudinalPosition\":222, \"lateralPosition\":3333 }, {\"sequence\":2, \"longitudinalPosition\":444, \"lateralPosition\":555 }, {\"sequence\":3, \"longitudinalPosition\":666, \"lateralPosition\":777 } ] }"
 */
void MockManager::on_calibrate(CalibrateEntity calibrate) {
    LOG(INFO) << "MockManager | on_calibrate";
    CalibrateManager::get_instance()->reinit(calibrate);
}
