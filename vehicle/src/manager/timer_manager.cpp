﻿#include <glog/logging.h>

#include "manager/timer_manager.h"
#include "manager/config_manager.h"
#include "manager/datakeeper.h"
#include "manager/mqtt_manager.h"

#include "manager/mock_manager.h"

TimerManager::TimerManager()
{
    result_timer = new QTimer(this);
    status_timer = new QTimer(this);

    connect(result_timer, SIGNAL(timeout()), this, SLOT(on_result_timer()));
    connect(status_timer, SIGNAL(timeout()), this, SLOT(on_status_timer()));

    connect(this, SIGNAL(reset_timer_sig()), this, SLOT(reset_timer_slot()));
}

TimerManager* TimerManager::get_instance() {
    static TimerManager instance;
    return &instance;
}

void TimerManager::start_all_timer() {
    LOG(INFO) << "TimerManager | start_all_timer";
    start_result_timer();
    start_status_timer();
}

void TimerManager::start_result_timer() {
    AppConfig *appConfig = ConfigManager::getInstance()->get_app_config();
    if (appConfig->algorithm_type != ALGORITHM_TYPE_VECHICLE_PEDESTRIAN) {
        LOG(INFO) << "TimerManager | start_timer | algorithm_type" 
            << appConfig->algorithm_type << ", result timer not start";
        return;
    }
    
    ConfigEntity *config = ConfigManager::getInstance()->get_ecb_config();
    if (config->time_cycle > 0) {
        LOG(INFO) << "TimerManager | start_timer | time_cycle" << config->time_cycle;
        result_timer->start(config->time_cycle);
    }
}

void TimerManager::reset_all_timer() {
   LOG(INFO) << "TimerManager | reset_all_timer";
   emit reset_timer_sig();
}

void TimerManager::start_status_timer() {
    ConfigEntity *config = ConfigManager::getInstance()->get_ecb_config();
    if (config->status_interval > 0) {
        LOG(INFO) << "TimerManager | start_timer | status_interval" << config->status_interval;
        status_timer->start(config->status_interval);
    }
}

void TimerManager::on_result_timer() {
    // LOG(INFO) << "TimerManager | on_result_timer";
    AppConfig *appConfig = ConfigManager::getInstance()->get_app_config();
    if (appConfig->algorithm_type != ALGORITHM_TYPE_VECHICLE_PEDESTRIAN) {
        LOG(INFO) << "TimerManager | on_result_timer | algorithm_type" 
            << appConfig->algorithm_type << ", on_result_timer not exec";
        return;
    }
    ResultEntity *result = DataKeeper::get_instance()->get_result();
    result->traffic_condition = 2;
    result->vehicle_types.passenger_cars = 2;
    result->vehicle_types.trucks = 2;
    result->weather = "sunny";
    MqttManager::getInstance()->send_result(result);
    // 检查当前车辆的数量是否超过设定的阈值
    if (result->vehicle_num >= MAX_RESULT_ID) {
        LOG(INFO) << "TimerManager | on_result_timer | vehicle_num exceed "
                  << MAX_RESULT_ID << " reset to ZERO";
        DataKeeper::get_instance()->clear_result();
        DataKeeper::get_instance()->clear_last_frame();
    }
}

void TimerManager::on_status_timer() {
    // send status
    ConfigEntity *pConfig = ConfigManager::getInstance()->get_ecb_config();
    StatusEntity status;
    status.time_cycle = pConfig->time_cycle;
    status.status = 0;
    status.mode = 0;
    MqttManager::getInstance()->send_status(&status);
}

void TimerManager::reset_timer_slot() {
    AppConfig *appConfig = ConfigManager::getInstance()->get_app_config();
    ConfigEntity *config = ConfigManager::getInstance()->get_ecb_config();

    if (appConfig->algorithm_type == ALGORITHM_TYPE_VECHICLE_PEDESTRIAN) {
        if (config->time_cycle > 0) {
            if (result_timer->isActive()) {
                result_timer->stop();
            }
            LOG(INFO) << "TimerManager | reset_timer_slot | time_cycle" << config->time_cycle;
            result_timer->start(config->time_cycle);
        }
    } else {
        LOG(INFO) << "TimerManager | reset_timer_slot | algorithm_type" 
            << appConfig->algorithm_type << ", result timer slot not reset";
    }

    if (config->status_interval > 0) {
        if (status_timer->isActive()) {
            status_timer->stop();
        }
        LOG(INFO) << "TimerManager | reset_timer_slot | status_interval" << config->status_interval;
        status_timer->start(config->status_interval);
    }
}

