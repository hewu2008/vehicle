#ifndef MQTTOBSERVER_H
#define MQTTOBSERVER_H

#include <QObject>

#include "entity/mqtt_entity.h"

class IMqttListener {
public:
    virtual void on_config(ConfigEntity config) = 0;

    virtual void on_command(CommandEntity command) = 0;

    virtual void on_update(char *archive, int len) = 0;

    virtual void on_calibrate(CalibrateEntity calibrate) = 0;
};


class MqttObserver : public QObject
{
    Q_OBJECT

public:
    static MqttObserver* get_instance();

    void set_listener(IMqttListener *listener);

    void notify_config(ConfigEntity config);

    void notify_command(CommandEntity command);

    void notify_update(char *archive, int len);

    void notify_calibrate(CalibrateEntity calibrate);

private:
    MqttObserver();

    IMqttListener *listener;

};

#endif // MQTTOBSERVER_H
