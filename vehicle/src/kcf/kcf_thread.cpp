#include <glog/logging.h>
#include "kcf/kcf_thread.h"
#include "detect/result_manager.h"

KCFThread::KCFThread() {
    moveToThread(this);
}

void KCFThread::run() {
    LOG(INFO) << "KCFThread | run";
    exec();
}

void KCFThread::on_kcf_update(QSharedPointer<Mat> sp_frame, bool is_ssd_frame)
{
    // LOG(INFO) << "KCFThread | on_kcf_update";
    ResultManager *result_manager = ResultManager::get_instance();
    result_manager->update_buffer_kcf(*sp_frame, is_ssd_frame);
}
