#include <glog/logging.h>
#include <QJsonDocument>

#include "entity/mqtt_entity.h"
#include "manager/json_parser.h"
#include "manager/mqtt_observer.h"
#include "include/mqtt/mqtt_client_human.h"

#include "manager/mqtt_manager.h"

MqttClientHuman::MqttClientHuman(const char *id, const char *topic, const char *host, int port)
    : MqttClient(id, topic, host, port)
{
}

void MqttClientHuman::on_connect(int rc) {
    LOG(INFO) << "MqttClientHuman | on_connect | rc" << rc;
}

void MqttClientHuman::on_message(const struct mosquitto_message *message) {
    LOG(INFO) << "MqttClientHuman | on_message";
}

void MqttClientHuman::on_subscribe(int mid, int qos_count, const int *granted_qos) {
    LOG(INFO) << "MqttClientHuman | on_subscribe Success";
}
